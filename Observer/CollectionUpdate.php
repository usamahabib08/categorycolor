<?php

namespace Sanipex\CategoryColor\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class CollectionUpdate implements ObserverInterface {
//    /**
//     * @param \Magento\Framework\UrlInterface $url
//     * @param \Magento\Framework\App\Response\Http $http
//     * @param \Magento\Customer\Model\Session $customerSession
//     */
//    public function __construct() {
//        
//
////        $this->customerSession = $customerSession;
//    }

    /**
     * Manages redirect
     */
    public function execute(Observer $observer) {
        $collection = $observer->getEvent()->getCollection();
        $collection->addAttributeToSelect('category_color');
        return $this;
    }

}

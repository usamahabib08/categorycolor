<?php
namespace Sanipex\CategoryColor\Model\Category;
  
class DataProvider extends \Magento\Catalog\Model\Category\DataProvider
{
  
    protected function getFieldsMap()
    {
        $fields = parent::getFieldsMap();
        $fields['content'][] = 'category_color'; // custom image field
         
        return $fields;
    }
}